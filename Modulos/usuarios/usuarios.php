<?php
/*
 * usuarios.php
 * 
 * Una consulta SQL para generar un listado.
 * 
 * @author Alex Barrios
 * @version 1.0
 * 
 * */
 
include 'lib/db.php';
include 'lib/sesion.php'; 

// Conexión a la base de datos
$enlace = conectar();

$rs     = mysql_query('SELECT * FROM usuario',$enlace);
$count  = mysql_num_rows($rs);
 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
        <meta http-equiv="content-type" 
              content="text/html;charset=utf-8">
		<title>Listado de Usuarios</title>
        <link rel="stylesheet" type="text/css" 
              href="css/general.css">
	</head>
	<body>
    
        <div id="area">
        
            <?php
            include 'parts/menu.php';
            ?>
            
            <div id="principal">

                <div class="navbar">
                    <div style="text-align:right">
                        <a href="usuarios_ins.php">Insertar</a> |
                        <a href="usuarios.php">Listar</a>
                    </div>
                </div>
                
                <table cellspacing="0" cellpadding="5" width="100%">
                    <tr style="color:#ffffff;background-color:#3F3F3F">
                        <td><strong>Código</strong></td>
                        <td><strong>Nombre</strong></td>
                        <td>                       </td>
                    </tr>
                    <?php
                    while ($filas = mysql_fetch_array($rs)) {
                        echo "<tr>
                                <td>{$filas['cod_usuario']}</td>
                                <td>{$filas['nombre']}</td>
                                <td> <a href=\"usuarios_eli.php?cod_usuario={$filas['cod_usuario']}\"><img src='img/eliminar.gif'</td>
                              </tr>";}
                    ?>
                </table>
                
            </div>
        
        </div>
                    
	</body>
</html>
<?php
// Cierre de conexión 
desconectar($enlace);
?>
