<?php
/*
 * usuarios_ins.php
 * 
 * Un formulario en HTML guardando los datos
 * en una base de datos MySQL con PHP.
 * 
 * @author Alex Barrios
 * @version 1.0
 * 
 * */
 
include 'lib/db.php';
include 'lib/sesion.php'; 
 
// Conexión a la base de datos

$enlace  = conectar();
$mensaje = '';

// Procesamiento del formulario
if (!empty($_POST['nombre'])) {

    $rs1 = mysql_query("INSERT INTO 
                        usuario(nombre,clave) 
                        VALUES ('{$_POST['nombre']}','".md5($_POST['clave'])."')");
    
    if (!$rs1)
        die('Query no valida: ' . mysql_error());
    else
        $mensaje = 'Registro insertado con éxito!';
        
}
 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
        <meta http-equiv="content-type" 
              content="text/html;charset=utf-8">
        <link rel="stylesheet" type="text/css" 
              href="css/general.css">
		<title>Insertar Usuarios</title>
	</head>
	<body>
    
        <div id="area">
        
            <?php
            include 'parts/menu.php';
            ?>
            
            <div id="principal">
                <div class="navbar">
                    <div style="text-align:right">
                        <a href="usuarios_ins.php">Insertar</a> |
                        <a href="usuarios.php">Listar</a>
                    </div>
                </div>
                
                <form name="Registro" action="?"
                      method="POST">
                
                <table cellspacing="0" cellpadding="1" width="100%">
                    <tr>
                        <td colspan="2" style="text-align:center;color:blue">
                            <?=$mensaje?>
                        </td>
                    </tr>
                    <tr>
                        <td>Nombre</td>
                        <td>
                            <input type="text" name="nombre" autofocus>
                        </td>
                    </tr>
                    <tr>
                        <td>Clave</td>
                        <td>
                            <input type="password" name="clave">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" value="Enviar">
                            <input type="reset" value="Limpiar">
                        </td>
                    </tr>
                </table>
                
                </form>
            
            </div>
        
        </div>
        
	</body>
</html>
<?php
// Cierre de conexión 
desconectar($enlace); 
?>
