<?php 	include "../../lib/sesion.php";
	include '../../Connections/atletas.php';
		
	include_once "../../class/class.php";
	include_once "../../class/funciones.php";
	include_once "../../class/class_buscar.php";
	
	?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cumplimiento de Registro Atletas</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="@Ray_saracual">

    <!-- Le styles -->
    <link href="../../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../css/normalize.css">
     <link href="../../css/bootstrap-responsive.css" rel="stylesheet">
     <link rel="stylesheet" type="text/css" href="../../css/imprimir-estadistica.css" media="print"/>
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="../../css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../ico/apple-touch-icon-57-precomposed.png">

    
     <link rel="stylesheet" type="text/css" href="../../css/redmond/jquery-ui.css">
 
  <script src="../../js/jquery-1.11.1.min.js"></script>
     
     	<link rel="shortcut icon" href="../../ico/favicon.png">
   
 
</head>

<?php

 
	mysql_select_db("sarat",$enlace) or die("Problema al seleccionar BD");	
	
	
	  


	$total_atletas = "SELECT * FROM atletas";
	$total = mysql_query($total_atletas) or die(mysql_error()); 
     $totalRows = mysql_num_rows($total);
	  
			
//SELECION ATLETAS POR PARROQUIAS GENERAL 					
$query_D = "SELECT COUNT(*) AS Filas,pq.parroquia 
FROM   parroquias AS pq JOIN atletas  AS atle ON atle.id_parroquia= pq.id_parroquia

GROUP BY  atle.id_parroquia
ORDER BY Filas DESC ";

$D = mysql_query($query_D) or die(mysql_error()); 


$totalRows_D = mysql_num_rows($D);

//SELECION ATLETAS POR DISCIPLINA 	DETALLADO				
$query_D1 = "SELECT COUNT(*) AS total_disc,disciplina,pq.parroquia,disc.disciplina
FROM   atletas AS atle JOIN disciplinas as disc ON atle.id_disciplina = disc.id_disciplina JOIN parroquias AS pq ON atle.id_parroquia = pq.id_parroquia


GROUP BY atle.id_disciplina,pq.parroquia
ORDER BY pq.parroquia ASC "

 
; 
$D1 = mysql_query($query_D1) or die(mysql_error()); 


$totalRows_D1 = mysql_num_rows($D1);





if ($totalRows_D>0 && $totalRows_D1 >0 ){

?>
<body>
<div id="area">


<div id="menu">
<?php include '../../menu/m_estadistica.php'; ?>
</div><!-- CIERRO MENÚ -->

<div id="encabezado">
 <table class="table table-bordered">
                  <tr class="well">
                    <td>
                    	<h2 align="center" style="font-style:italic;">
                        
                          <p> Cumplimiento de Registro General Atletas<br /> 
                          
                         <?php //echo $row_avg['average']
;	?>
                           </p>
    </h2>
                    </td>
                  </tr>
</table>
</div><!-- ENCABEZADO -->
 <div align="center">
<div id="leyenda"  style="padding:1%; display:inline-block; ">

<p style="font-style:italic; font-weight:bold;">
                         
 Fuente : Sala Situacional Gobernacion del Estado Vargas.
 Hora de Reporte: <span style="color:red; "><?php 
 echo date("F j, Y, g:i a");?></span>

 </p>
 </div><!-- LEYENDA -->
   </div><!-- ENCABEZADO-->             
  <div id="estadistica_general"> 
    <table width="50%" class="table table-bordered">
  <tr class="well">
    <th width="21%" scope="col"><div align="center">ITÉMS</div></th>
    <th width="68%" scope="col"><div align="center">PARROQUIA</div></th>
    <th width="21%" scope="col"><div align="center">ATLETAS REGISTRADOS</div></th>
    <th width="21%" scope="col"><div align="center">ATLETAS ESTIMADOS </div></th>
     <th width="21%" scope="col"><div align="center">DIFERENCIA </div></th>
 <?php
 
 
$cont=0;
while($row=mysql_fetch_array($D) )
{
	$cont++
   ?> 
   </tr>
    <tr  class="brillo">
     <th><?php echo $cont ?></th>
    <th width="68%" scope="col"><?php echo $row['parroquia'] ?></th>
    <th width="21%" scope="col"><?php echo $row['Filas']?></th>
     <th width="21%" scope="col"> 82</th>
     <th width="21%" scope="col"><?php 
	 $diferencia =  82 - $row['Filas'];
	 echo $diferencia; ?></th>
     
    </tr>
    
   

<?php }?>


 
  <th colspan="2" scope="col"> <div align="center" style="padding:1%">TOTALES</div></th>
   <th width="17%" scope="col"> <h4 align="center" style="color:#00F"><?php echo $totalRows ?></h4></th>
    <th width="17%" scope="col"> <h4 align="center" style="color:#00F"> 902</h4></th>
     <th width="17%" scope="col"> <h4 align="center" style="color:#00F"><?php $diferencia_total =902-$totalRows; 
	 echo $diferencia_total;?> </h4></th>
     
      </table> 
      
 
</div><!-- CIERRO GENERAL-->
 <h5 style=" font-style:italic; text-align: center; margin-bottom:5%; "> 
   Con un  Participacion igual a : <span style="color:#F00; font-size:16px"><?php $participacion =$totalRows*100/902;
	 echo number_format ($participacion)."%".'</span>'." . En base al 100% esperado, representado por 902 Atletas, según estimados de IDEAFE.";  ?>      
</h5>
<table class="table table-bordered">
                  <tr class="well">
                    <td>
                    	<h2 align="center" style="font-style:italic;">
                        
                          <p> Cumplimiento de Registro Detallado Atletas </p>
    </h2>
                       
                    </td>
                  </tr>
</table>


<section id="estadistica_detallada">
              
  <table width="50%" class="table table-bordered">
  <tr class="well">
    <th width="5%" scope="col"><div align="center">ITÉMS</div></th>
    <th width="35%" scope="col"><div align="center">PARROQUIA</div></th>
    <th width="10%" scope="col"><div align="center">ATLETAS</div></th>
    <th width="50%" scope="col"><div align="center">DISCIPLINA</div></th>
    
  <?php
  
    
  $cont=0;
while($row=mysql_fetch_array($D1)  )
{
	$cont++
   ?> 
    </tr>
        
         
 <tr class="brillo">
   <th><?php echo $cont ?></th>
    <th width="35%" scope="col"><?php echo $row['parroquia'] ?></th>
    <th width="10%" scope="col"><?php echo $row['total_disc']?></th>
    <th width="50%" ><?php echo $row['disciplina'] ?></th>
    
 <?php	}?>
  </tr>  
 
</table>


</section><!-- CIERRRO DETALLADA-->
<div align="center">
 <a class="btn" onclick="window.print()"><i class="icon-print" ></i> Imprimir</a>
</div>
   
      <span class="boton-top">▲</span>
 
<script>
	$(window).scroll(function(){
	    if ($(this).scrollTop() > 0) {
	        $('.boton-top').fadeIn();
	    } else {
	        $('.boton-top').fadeOut();
	    }
	});

	$('.boton-top').click(function(){
	    $(document.body).animate({scrollTop : 0}, 500);
	    return false;
	});
	</script>
    
<?php } else {
?>
	<article  style=" text-align:center; padding-top:15%;">
    <img src="../../img/no-atletas.png" />
</article>
	
<?php	} ?>
    
   </div><!-- AREA -->
   
   
   
    <!-- Le javascript ../../js/jquery.js
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../js/bootstrap-transition.js"></script>
    <script src="../../js/bootstrap-alert.js"></script>
    <script src="../../js/bootstrap-modal.js"></script>
    <script src="../../js/bootstrap-dropdown.js"></script>
    <script src="../../js/bootstrap-scrollspy.js"></script>
    <script src="../../js/bootstrap-tab.js"></script>
    <script src="../../js/bootstrap-tooltip.js"></script>
    <script src="../../js/bootstrap-popover.js"></script>
    <script src="../../js/bootstrap-button.js"></script>
    <script src="../../js/bootstrap-collapse.js"></script>
    <script src="../../js/bootstrap-carousel.js"></script>
    <script src="../../js/bootstrap-typeahead.js"></script>

   
</body>
</html>